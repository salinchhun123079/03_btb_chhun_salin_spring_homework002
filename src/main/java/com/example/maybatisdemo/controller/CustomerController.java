package com.example.maybatisdemo.controller;

import com.example.maybatisdemo.model.entity.Customer;
import com.example.maybatisdemo.model.entity.Product;
import com.example.maybatisdemo.model.request.CustomerRequest;
import com.example.maybatisdemo.model.request.ProductRequest;
import com.example.maybatisdemo.model.response.ProductResponse;
import com.example.maybatisdemo.services.serviceImp.CustomerService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

@RestController
@RequestMapping("/api/v1/customers")
public class CustomerController {
    private final CustomerService customerService;
    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }
    @GetMapping("/all")
    @Operation(summary = "Get all customer")
    public ResponseEntity<List<Customer>> getAllCustomer(){
        return ResponseEntity.ok(customerService.getAllCustomer());
    }

    @GetMapping("/{customerId}")
    @Operation(summary = "Get customer by id")
    public ResponseEntity<ProductResponse<Customer>> getProductById(@PathVariable("customerId") Integer customerId){
        ProductResponse<Customer> response = null;
        if (customerService.getCustomerById(customerId) != null){
            response = ProductResponse.<Customer>builder()
                    .payload(customerService.getCustomerById(customerId))
                    .message("Get customer by id successfully")
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }else {
            response = ProductResponse.<Customer>builder()
                    .message("Data not found")
                    .httpStatus(HttpStatus.NOT_FOUND)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.badRequest().body(response);
        }
    }

    @DeleteMapping("/{customerId}")
    @Operation(summary = "Delete customer by id")
    public ResponseEntity<ProductResponse<String>> deleteById(@PathVariable("customerId") Integer customerId){
        ProductResponse<String> response = null;
        if (customerService.deleteCustomerById(customerId) == true){
            response = ProductResponse.<String>builder()
                    .message("Delete successfully")
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }else {
            response = ProductResponse.<String>builder()
                    .message("No contain data to delete")
                    .httpStatus(HttpStatus.NOT_FOUND)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.badRequest().body(response);
        }
    }

    @PostMapping()
    @Operation(summary = "Insert customer")
    public ResponseEntity<ProductResponse<Customer>> insertCustomer(@RequestBody CustomerRequest customerRequest){
        Integer storeCustomerId = customerService.insertCustomer(customerRequest);
        if (storeCustomerId != null){
            ProductResponse<Customer> response = ProductResponse.<Customer>builder()
                    .payload(customerService.getCustomerById(storeCustomerId))
                    .message("Insert successfully")
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }
        return null;
    }

    @PutMapping("/{customerId}")
    @Operation(summary = "Update customer by id")
    public ResponseEntity<ProductResponse<Customer>> updateCustomerById(
            @RequestBody CustomerRequest customerRequest, @PathVariable("customerId") Integer customerId
    ){
        ProductResponse<Customer> response = null;
        Integer idCustomerUpdate = customerService.updateCustomer(customerRequest, customerId);
        if (idCustomerUpdate != null){
            response = ProductResponse.<Customer>builder()
                    .payload(customerService.getCustomerById(idCustomerUpdate))
                    .message("Updated successfully")
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }else {
            response = ProductResponse.<Customer>builder()
                    .message("No contain data to update")
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.badRequest().body(response);
        }
    }
}
