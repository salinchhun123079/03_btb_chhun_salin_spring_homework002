package com.example.maybatisdemo.controller;

import com.example.maybatisdemo.model.entity.Customer;
import com.example.maybatisdemo.model.entity.Invoice;
import com.example.maybatisdemo.model.entity.Product;
import com.example.maybatisdemo.model.request.CustomerRequest;
import com.example.maybatisdemo.model.request.InvoiceRequest;
import com.example.maybatisdemo.model.request.ProductRequest;
import com.example.maybatisdemo.model.response.ProductResponse;
import com.example.maybatisdemo.services.serviceImp.InvoiceService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

@RestController
@RequestMapping("/api/v1/invoices")

public class InvoiceController {
    private final InvoiceService invoiceService;

    public InvoiceController(InvoiceService invoiceService) {
        this.invoiceService = invoiceService;
    }
    @GetMapping("/all")
    @Operation(summary = "Get all invoice")
    public ResponseEntity<List<Invoice>> getAllInvoice(){
        return ResponseEntity.ok(invoiceService.getAllInvoice());
    }

    @GetMapping("/{invoiceId}")
    @Operation(summary = "Get invoice by id")
    public ResponseEntity<Invoice> getAllInvoiceById(@PathVariable("invoiceId") Integer invoiceId){
        return ResponseEntity.ok(invoiceService.getInvoiceById(invoiceId));
    }

    @DeleteMapping("/{invoiceId}")
    @Operation(summary = "Delete invoice by id")
    public ResponseEntity deleteInvoiceById(@PathVariable("invoiceId") Integer invoiceId){
        return ResponseEntity.ok(invoiceService.deleteInvoiceById(invoiceId));
    }

    @PostMapping()
    @Operation(summary = "Insert invoice")
    public ResponseEntity<ProductResponse<Invoice>> insertInvoice(@RequestBody InvoiceRequest invoiceRequest){
        Integer storeInvoiceId = invoiceService.insertInvoice(invoiceRequest);
        if (storeInvoiceId != null){
            ProductResponse<Invoice> response = ProductResponse.<Invoice>builder()
                    .payload(invoiceService.getInvoiceById(storeInvoiceId))
                    .message("Insert successfully")
                    .httpStatus(HttpStatus.OK)
                    .build();
            return ResponseEntity.ok(response);
        }
        return null;
    }


    @PutMapping("/{invoiceId}")
    @Operation(summary = "Update invoice by id")
    public ResponseEntity<ProductResponse<Invoice>> updateInvoiceById(
            @RequestBody InvoiceRequest invoiceRequest, @PathVariable("invoiceId") Integer invoiceId
    ){
                Integer storeInvoiceId = invoiceService.insertInvoice(invoiceRequest);
        if (storeInvoiceId != null){
            ProductResponse<Invoice> response = ProductResponse.<Invoice>builder()
                    .payload(invoiceService.getInvoiceById(storeInvoiceId))
                    .message("Insert successfully")
                    .httpStatus(HttpStatus.OK)
                    .build();
            return ResponseEntity.ok(response);
        }
        return null;
    }

}
