package com.example.maybatisdemo.controller;

import com.example.maybatisdemo.model.entity.Product;
import com.example.maybatisdemo.model.request.ProductRequest;
import com.example.maybatisdemo.model.response.ProductResponse;
import com.example.maybatisdemo.services.serviceImp.ProductService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.models.security.SecurityScheme;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

@RestController
@RequestMapping("/api/v1/products")
public class ProductController {
    private final ProductService productService;
    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping("/all")
    @Operation(summary = "Get all products")
    public ResponseEntity<ProductResponse<List<Product>>> getAllProduct(){
        ProductResponse<List<Product>> response = ProductResponse.<List<Product>>builder()
                .payload(productService.getAllProduct())
                .message("Fetch all product successfully")
                .httpStatus(HttpStatus.OK)
                .timestamp(new Timestamp(System.currentTimeMillis()))
                .build();
        return ResponseEntity.ok(response);
    }

    @GetMapping("/{productId}")
    @Operation(summary = "Get product by uid")
    public ResponseEntity<ProductResponse<Product>> getProductById(@PathVariable("productId") Integer productId){
        ProductResponse<Product> response = null;
        if (productService.getProductById(productId) != null){
            response = ProductResponse.<Product>builder()
                    .payload(productService.getProductById(productId))
                    .message("Get product by id successfully")
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);

        }else {
            response = ProductResponse.<Product>builder()
                    .message("Data not found")
                    .httpStatus(HttpStatus.NOT_FOUND)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.badRequest().body(response);
        }
    }

    @DeleteMapping("/{productId}")
    @Operation(summary = "Delete product by id")
    public ResponseEntity<ProductResponse<String>> deleteById(@PathVariable("productId") Integer productId){
        ProductResponse<String> response = null;
        if (productService.deleteProductById(productId) == true){
            response = ProductResponse.<String>builder()
                    .message("Delete successfully")
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }else {
            response = ProductResponse.<String>builder()
                    .message("No contain data to delete")
                    .httpStatus(HttpStatus.NOT_FOUND)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.badRequest().body(response);
        }
    }

    @PostMapping()
    @Operation(summary = "Insert product")
    public ResponseEntity<ProductResponse<Product>> insertProduct(@RequestBody ProductRequest productRequest){
        Integer storeProductId = productService.insertProduct(productRequest);
        if (storeProductId != null){
            ProductResponse<Product> response = ProductResponse.<Product>builder()
                    .payload(productService.getProductById(storeProductId))
                    .message("Insert successfully")
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }
        return null;
    }

    @PutMapping("/{productId}")
    @Operation(summary = "Update product by id")
    public ResponseEntity<ProductResponse<Product>> updateProductById(
            @RequestBody ProductRequest productRequest, @PathVariable("productId") Integer productId
    ){
        ProductResponse<Product> response = null;
        Integer idProductUpdate = productService.updateProduct(productRequest, productId);
        if (idProductUpdate != null){
            response = ProductResponse.<Product>builder()
                    .payload(productService.getProductById(idProductUpdate))
                    .message("Updated successfully")
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }else {
            response = ProductResponse.<Product>builder()
                    .message("No contain data to update")
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.badRequest().body(response);
        }
    }

}
