package com.example.maybatisdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MaybatisDemoApplication {

	public static void main(String[] args) {

		SpringApplication.run(MaybatisDemoApplication.class, args);
	}

}
