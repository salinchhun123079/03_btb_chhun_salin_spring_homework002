package com.example.maybatisdemo.services.serviceImp;

import com.example.maybatisdemo.model.entity.Customer;
import com.example.maybatisdemo.model.request.CustomerRequest;
import com.example.maybatisdemo.repository.CustomerRepository;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class CustomerServiceImp implements CustomerService{
    private final CustomerRepository customerRepository;

    public CustomerServiceImp(CustomerRepository customerRepository) {

        this.customerRepository = customerRepository;
    }

    @Override
    public List<Customer> getAllCustomer() {
        return customerRepository.findAllCustomer();
    }

    @Override
    public Customer getCustomerById(Integer customerId) {
        return customerRepository.getCustomerById(customerId);
    }

    @Override
    public boolean deleteCustomerById(Integer customerId) {
        return customerRepository.deleteProductById(customerId);
    }

    @Override
    public Integer insertCustomer(CustomerRequest customerRequest) {
        Integer customerId = customerRepository.insertCustomer(customerRequest);
        return customerId;
    }

    @Override
    public Integer updateCustomer(CustomerRequest customerRequest, Integer customerId) {
        Integer customerIdUpdate = customerRepository.updateCustomer(customerRequest, customerId);
        return customerIdUpdate;
    }
}
