package com.example.maybatisdemo.services.serviceImp;

import com.example.maybatisdemo.model.entity.Customer;
import com.example.maybatisdemo.model.request.CustomerRequest;
import com.example.maybatisdemo.model.request.ProductRequest;

import java.util.List;

public interface CustomerService {
    //    select all customer
    List<Customer> getAllCustomer();

    //    select customer by id
    Customer getCustomerById(Integer customerId);

    //    delete customer by id
    boolean deleteCustomerById(Integer customerId);

    //    insert customer
    Integer insertCustomer(CustomerRequest productRequest);

    //    update customer
    Integer updateCustomer(CustomerRequest customerRequest, Integer customerId);
}
