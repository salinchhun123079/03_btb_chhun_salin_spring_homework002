package com.example.maybatisdemo.services.serviceImp;

import com.example.maybatisdemo.model.entity.Product;
import com.example.maybatisdemo.model.request.ProductRequest;
import com.example.maybatisdemo.repository.ProductRepository;
import io.swagger.v3.oas.annotations.servers.Server;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class ProductServiceImp implements ProductService{
    private final ProductRepository productRepository;

    public ProductServiceImp(ProductRepository productRepository) {
        this.productRepository=productRepository;
    }

//    @Override
//    public List<Product> getAllProduct() {
//        return productRepository.findAllProduct();
//    }
    @Override
    public List<Product> getAllProduct() {
        return productRepository.findAllProduct();
    }

    @Override
    public Product getProductById(Integer invoiceId) {
        return productRepository.getProductById(invoiceId);
    }

    @Override
    public boolean deleteProductById(Integer productId) {
        return productRepository.deleteProductById(productId);
    }

    @Override
    public Integer insertProduct(ProductRequest productRequest) {
        Integer productId = productRepository.insertProduct(productRequest);
        return productId;
    }

    @Override
    public Integer updateProduct(ProductRequest productRequest, Integer productId) {
        Integer productIdUpdate = productRepository.updateProduct(productRequest, productId);
        return productIdUpdate;
    }
}
