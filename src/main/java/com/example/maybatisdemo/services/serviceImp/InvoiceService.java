package com.example.maybatisdemo.services.serviceImp;

import com.example.maybatisdemo.model.entity.Invoice;
import com.example.maybatisdemo.model.request.InvoiceRequest;
import com.example.maybatisdemo.model.request.ProductRequest;

import java.util.List;

public interface InvoiceService {
    List<Invoice> getAllInvoice();

    Invoice getInvoiceById(Integer invoiceId);

    boolean deleteInvoiceById(Integer invoiceId);

    Integer insertInvoice(InvoiceRequest invoiceRequest);

    Integer updateInvoice(InvoiceRequest invoiceRequest, Integer invoiceId);

}
