package com.example.maybatisdemo.services.serviceImp;

import com.example.maybatisdemo.model.entity.Invoice;
import com.example.maybatisdemo.model.request.CustomerRequest;
import com.example.maybatisdemo.model.request.InvoiceRequest;
import com.example.maybatisdemo.repository.InvoiceRepository;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class InvoiceServiceImp implements InvoiceService{

    private final InvoiceRepository invoiceRepository;
    public InvoiceServiceImp(InvoiceRepository invoiceRepository) {

        this.invoiceRepository = invoiceRepository;
    }
    @Override
    public List<Invoice> getAllInvoice() {
        return invoiceRepository.findAllInvoice();
    }

    @Override
    public Invoice getInvoiceById(Integer invoiceId) {
        return invoiceRepository.getInvoiceById(invoiceId);
    }

    @Override
    public Integer insertInvoice(InvoiceRequest invoiceRequest) {
        Integer invoiceId = invoiceRepository.insertInvoice(invoiceRequest);
        return invoiceId;
    }

    @Override
    public Integer updateInvoice(InvoiceRequest invoiceRequest, Integer invoiceId) {
        Integer invoiceIdUpdate = invoiceRepository.updateInvoice(invoiceRequest, invoiceId);
        return invoiceIdUpdate;
    }


    @Override
    public boolean deleteInvoiceById(Integer invoiceId) {
        return invoiceRepository.deleteInvoiceById(invoiceId);
    }


}
