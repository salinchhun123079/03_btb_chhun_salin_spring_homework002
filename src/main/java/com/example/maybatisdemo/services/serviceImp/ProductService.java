package com.example.maybatisdemo.services.serviceImp;

import com.example.maybatisdemo.model.entity.Product;
import com.example.maybatisdemo.model.request.ProductRequest;

import java.util.List;

public interface ProductService {

    //    select all product
    List<Product> getAllProduct();

    //    select product byId
    Product getProductById(Integer productId);

    //    delete product
    boolean deleteProductById(Integer productId);

    //    insert product
    Integer insertProduct(ProductRequest productRequest);

    //    update product
    Integer updateProduct(ProductRequest productRequest, Integer productId);

}
