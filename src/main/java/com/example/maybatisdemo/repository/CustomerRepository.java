package com.example.maybatisdemo.repository;

import ch.qos.logback.core.model.INamedModel;
import com.example.maybatisdemo.model.entity.Customer;
import com.example.maybatisdemo.model.request.CustomerRequest;
import com.example.maybatisdemo.model.request.ProductRequest;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface CustomerRepository {
    @Select("SELECT * FROM customer_tb")
    List<Customer> findAllCustomer();

    @Select("SELECT * FROM customer_tb WHERE customer_id = #{customerId}")
    Customer getCustomerById(Integer customerId);

    @Delete("DELETE FROM customer_tb WHERE customer_id = #{customerId}")
    boolean deleteProductById(@Param("customerId") Integer customerId);

    @Select("INSERT INTO customer_tb (customer_name, customer_address, customer_phone) VALUES(#{request.customer_name}, #{request.customer_address}, #{request.customer_phone}) RETURNING customer_id")
    Integer insertCustomer(@Param("request") CustomerRequest customerRequest);

    @Select("UPDATE customer_tb " +
            "SET customer_name = #{request.customer_name}, " +
            "customer_address = #{request.customer_address}, " +
            "customer_phone = #{request.customer_phone} " +
            "WHERE customer_id = #{customerId} " +
            "RETURNING customer_id")
    Integer updateCustomer(@Param("request") CustomerRequest customerRequest, Integer customerId);

}
