package com.example.maybatisdemo.repository;

import com.example.maybatisdemo.model.entity.Invoice;
import com.example.maybatisdemo.model.request.CustomerRequest;
import com.example.maybatisdemo.model.request.InvoiceRequest;
import com.example.maybatisdemo.model.request.ProductRequest;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Scanner;

@Mapper
public interface InvoiceRepository {

    //query all invoice
    @Select("SELECT * FROM invoice_tb")
    @Results(
            id = "invoice mapper",
            value = {
                    @Result(property = "invoice_id", column = "invoice_id"),
                    @Result(property = "customer", column = "customer_id",
                            one = @One(select = "com.example.maybatisdemo.repository.CustomerRepository.getCustomerById")
                    ),
                    @Result(property = "products", column = "invoice_id",
                            many = @Many(select = "com.example.maybatisdemo.repository.ProductRepository.getProductById")
                    )
            }
    )
    List<Invoice> findAllInvoice();


    //query get invoiceById
    @Select("SELECT * FROM invoice_tb WHERE invoice_id = #{invoiceId}")
   @ResultMap("invoice mapper")
    Invoice getInvoiceById(Integer invoiceId);

    //query delete invoiceById
    @Delete("DELETE FROM invoice_tb WHERE invoice_id = #{invoiceId}")
    boolean deleteInvoiceById(@Param("invoiceId") Integer invoiceId);


    @Select("INSERT INTO invoice_tb (invoice_date, customer_id) VALUES(#{request.invoice_date}, #{request.customer_id}) RETURNING invoice_id")
    @Result(property = "customer", column = "customer_id",
            one = @One(select = "com.example.maybatisdemo.repository.CustomerRepository.getCustomerById")
    )
    Integer insertInvoice(@Param("request") InvoiceRequest invoiceRequest);


    @Select("UPDATE invoice_tb " +
            "SET customer_id = #{request.invoice_id}, " +
            "product_id = #{request.product_id}, " +
            "WHERE invoice_id = #{invoiceId} " +
            "RETURNING invoice_id")
    Integer updateInvoice(@Param("request") InvoiceRequest invoiceRequest, Integer invoiceId);

}
