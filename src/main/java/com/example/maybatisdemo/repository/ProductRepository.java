package com.example.maybatisdemo.repository;

import com.example.maybatisdemo.model.entity.Product;
import com.example.maybatisdemo.model.request.ProductRequest;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface ProductRepository {

    @Select("SELECT * FROM product_tb")
    List<Product> findAllProduct();

    @Select("SELECT p.product_id, p.product_name, p.product_price FROM invoice_detail_tb id " +
            "INNER JOIN product_tb p ON p.product_id = id.product_id " +
            "WHERE id.invoice_id = #{invoiceId}")
    Product getProductById(Integer invoiceId);

    @Delete("DELETE FROM product_tb WHERE product_id = #{productId}")
    boolean deleteProductById(@Param("productId") Integer productId);

    @Select("INSERT INTO product_tb (product_name, product_price) VALUES(#{request.product_name}, #{request.product_price}) RETURNING product_id")
    Integer insertProduct(@Param("request") ProductRequest productRequest);

    @Select("UPDATE product_tb " +
            "SET product_name = #{request.product_name}, " +
            "product_price = #{request.product_price} " +
            "WHERE product_id = #{productId} " +
            "RETURNING product_id")
    Integer updateProduct(@Param("request") ProductRequest productRequest, Integer productId);
}
